<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PostRepository;
use App\Form\PostType;
use App\Entity\Post;


class PostController extends AbstractController
{

    /**
     * @Route ("/", name="index")
     */

    public function index(PostRepository $repo)
     {
        $tab = $repo->findLastRows();
        // dump($tab);
        return $this->render("index.html.twig", [
            "tab" => $tab,


        ]);
     }
    /**
     * @Route ("/add-post", name="add-post")
     */

    public function postForm(Request $request, PostRepository $repo)
    {
        dump($repo->findAll());
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($post);            
            return $this->redirectToRoute("all-posts");
            dump($post);
        }

        return $this->render("add-post.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/show-post/{id}", name="show-post")
     */
    public function showPost(PostRepository $repo, int $id)
    {
        $post = $repo->find($id);
        dump($post);
        return $this->render("show-post.html.twig", [
            "post" => $post
        ]);
    }

    /**
     * @Route ("/all-posts", name="all-posts")
     */
    public function allPosts(PostRepository $repo)
    {
        $tab = $repo->findAll();
        dump($tab);
        return $this->render("all-posts.html.twig", [
            "tab" => $tab
        ]);
    }


    /**
     * @Route ("/delete-post/{id}", name="delete-post")
     */
    public function deletePost(PostRepository $repo, int $id)
    {
        $post = $repo->find($id);
        dump($post);
        $repo->remove($post);

        return $this->redirectToRoute("all-posts");
    }

    /**
     * @Route ("/update-post/{id}", name="update-post")
     */
    public function updatePost(Request $request, PostRepository $repo, int $id)
    {
        
        $post = $repo->find($id);
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($post);           
            return $this->redirectToRoute("all-posts");
            dump($post);
        }

        return $this->render("update-post.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route ("/search-post", name="search_post")
     */
    public function searchPost(Request $request, PostRepository $repo)
    {
        
        $tabWords = $repo->search($request->get('search'));
       
        return $this->render("search-post.html.twig", [
            'tabWords' => $tabWords
        ]);

    }
}